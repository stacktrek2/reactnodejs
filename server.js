import express from 'express';
import morgan from 'morgan';
import todoRoutes from './routes/todo.js';

const app = express();

app.use(express.json())
app.use(morgan('dev'))

const server = app.listen(8085, function() {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Example app listening at http://%s:%s", host, port);

})

app.get('/', (req, res) => {
    res.send('Hello Worldasd')
})


app.use('/api/todos', todoRoutes)