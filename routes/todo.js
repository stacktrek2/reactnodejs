import express from 'express';
import { todoCollection } from '../db.js';
import { ObjectId } from 'mongodb';

const router = express.Router();

let todos = [
    { id: 1, task: 'Read Emails', checked: false },
    { id: 2, task: 'Fix the bed', checked: false }
];

// Get all Todos
router.get('/', async (req,res) => {
    // const todos = await todoCollection.find().toArray()
    res.status(200).send(todos);


})

// Get a single todo using ID
router.get('/:id', async (req,res) => {
    const taskId = req.params.id;
    const task = todos.find(todo => todo.id === parseInt(taskId));

    if(task){
        res.status(200).send(task)
    } else {
        res.status(404).json({error: 'Todo not found'})
    }
    // const foundTodoItem = await todoCollection.findOne({
    //     _id: new ObjectId(taskId)
    // })

    // if(foundTodoItem){
    //     res.status(200).send(foundTodoItem)
    // } else {
    //     res.status(404).json({error: 'Todo not found'})
    // }
})

// Create a todo

router.post('/', async (req,res) => {
    const { task } = req.body;

    if(!task) {
        res.status(400).json({ error: 'Invalid/Empty request body' });
        return;
    }

    const newTodoItem = { ...req.body, id: new Date().getTime(), checked: false }
    
    // const result = await todoCollection.insertOne(newTodoItem)

    // const todoItem = await todoCollection.findOne({
    //     _id: new ObjectId(result.insertedId)
    // })

    todos = [...todos, newTodoItem]
    res.status(201).send(newTodoItem)
})

// Update a todo check status using ID
router.patch('/:id', async (req,res) => {
    const taskId = req.params.id;
    const { checked } = req.body;
    const task = todos.find(todo => todo.id === parseInt(taskId));

    if(!checked) {
        res.status(400).json({ error: 'Invalid request, should be "checked" ' });
        return;
    }

    if(typeof checked !== 'boolean') {
        res.status(400).json({ error: 'Request body should be a boolean' });
        return;
    }
 
    if(task){
        task.checked = checked
        res.status(200).send(task)
    } else {
        res.status(404).json({error: 'Todo not found'})
    }
    // const task = req.body.task

    // if(!ObjectId.isValid(taskId)) {
    //     return res.status(400).send('ID is not valid')
    // }

    // if(!task) {
    //     res.status(400).json({ error: 'Invalid/Empty request body' });
    //     return;
    // }

    // const foundTodoItem = await todoCollection.findOne({
    //     _id: new ObjectId(taskId)
    // })

    // if(foundTodoItem == null) {
    //     return res.status(404).send('Not Found')
    // }

    // const result = await todoCollection.updateOne({
    //     _id: new ObjectId(taskId)
    // }, {
    //     $set: { task }
    // })

    // const updatedTodoItem = await todoCollection.findOne({
    //     _id: new ObjectId(taskId)
    // })

    // res.status(200).send(updatedTodoItem)
})

// Delete a todo item
router.delete('/:id', async (req,res)=> {
    const taskId = req.params.id
    const task = todos.find(todo => todo.id === parseInt(taskId));

    if(!task) {
        res.status(400).json({ error: 'Todo not Found / ID does not exist' });
        return;
    }
 
    if (task !== -1) {
        todos.splice(task, 1);
        res.sendStatus(204);
    } else {
        res.status(404).json({ error: 'Todo not found' });
    }
    // const foundTodoItem = await todoCollection.findOne({
    //     _id: new ObjectId(taskId)
    // })

    // if(foundTodoItem == null) {
    //     return res.status(404).send('Not Found')
    // }

    // await todoCollection.deleteOne(foundTodoItem)
    // res.status(204).send()
})

export default router;